from django.urls import reverse_lazy
from tasks.models import Task
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.list import ListView

# Create your views here.


class TaskCreateView(LoginRequiredMixin, CreateView):
    model = Task
    template_name = "tasks/new.html"
    fields = ["name", "start_date", "due_date", "project", "assignee"]
    context_object_name = "tast_create"

    def get_success_url(self) -> str:
        return reverse_lazy("show_project", args=[self.object.project.id])


class TaskListView(LoginRequiredMixin, ListView):
    model = Task
    template_name = "tasks/list.html"
    context_object_name = "task_list"


class TaskUpdateView(LoginRequiredMixin, UpdateView):
    model = Task
    context_object_name = "task_edit"
    fields = ["is_completed"]
    success_url = reverse_lazy("show_my_tasks")
